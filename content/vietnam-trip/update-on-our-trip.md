Title: Quick Update On Our Trip
Date: 2010-11-10 18:19
Author: sumner
Category: Trip to Vietnam
Slug: update-on-our-trip
Status: published

We made it back from the homestay safely. It was very fun but I can't
tell you about it now because we are going on another trek to Tavan to a
mountain lodge. Bye
