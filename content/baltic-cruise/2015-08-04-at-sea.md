Title: 8-4-2015 - At Sea
Date: 2015-08-04 12:56
Author: sumner
Category: Baltic Cruise and Northern Europe
Tags: At Sea
Slug: 8-4-2015-at-sea
Status: published

Today we were at sea, only sailing. I really don't want to write all about it,
so I won't. I will tell you that it was fun. Just combine what I've described to
you about the last few evenings on the ship and add some swimming to that and
you have a good idea of what happened today.
