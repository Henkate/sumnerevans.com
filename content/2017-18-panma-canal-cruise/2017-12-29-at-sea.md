Title: At Sea
Date: 2017-12-29 17:59
Author: Sumner Evans
Category: Panama Canal Cruise
Tags: At Sea, Ping Pong, Swimming, Soccer
Slug: 2017-12-29-at-sea
Status: published

Since we were at sea today with nothing in particular to do, I slept in quite a
bit. I woke up and went upstairs and ate breakfast and then played in the ping
pong tournament. I am decent at ping pong, but there are some really good
players on this cruise, so I'm happy when I win a game before losing.

Anyway, after that I spent the day swimming, playing ping pong and soccer, and
hanging out with people on the boat.

Then we went to dinner and the show (which was ok; it was just the Zuiderdam
singers and dancers and wasn't anything to write home about). After the show I
went to *BILLBOARD on Board* for a while before going to bed.
