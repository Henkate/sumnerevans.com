Title: Denver to Ft. Lauderdale
Date: 2017-12-26 08:00
Author: Sumner Evans
Category: Panama Canal Cruise
Tags: Denver, Colorado, Ft. Lauderdale, Florida
Slug: 2017-12-26-denver-to-ft-lauderdale
Status: published

We are going on another trip! Today we left for Ft. Lauderdale, Florida where we
will start our 12-day cruise!

Our flight didn't leave until 4:15, so we were able to finish packing up all of
our stuff this morning. Our trip to the airport and flight were uneventful. In
Ft. Lauderdale, it took a long time for the shuttle to the hotel to show up, we
had to wait for over an hour! We did make it to the hotel though, and we are
extremely tired so we are going to bed. Tomorrow should be more interesting.
