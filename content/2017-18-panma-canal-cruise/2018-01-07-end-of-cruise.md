Title: End of Cruise
Date: 2018-01-07 15:42
Author: Sumner Evans
Category: Panama Canal Cruise
Tags: Ft. Lauderdale, Florida, Denver, Colorado
Slug: 2018-01-07-end-of-cruise
Status: published

Today was the end of our cruise :(

I woke up rather early to go say goodbye to Alex and Eric.

[![Alex, Eric, and me]({static}/images/panama-canal-cruise/alex-eric.jpg)]({static}/images/panama-canal-cruise/alex-eric.jpg)

Then we went to eat breakfast. Then we finished packing our bags and then played
some ping pong while we waited to disembark. Eventually we just decided to play
stupid and just get off because we started to be concerned with getting to the
airport on time.  It worked, and it was a good thing because once we got off the
ship we had to wait in a massive queue at immigration. Still, we made it to the
airport with plenty of time.

The flight was a bit turbulent, and I swear there were babies in every row of
the plane so the crying levels were real. Good thing I'm just a big baby and
took a nap for most of the flight XD.

Well, we are home now, and it's back to school! I may also post a reflections
post if I feel like it.
