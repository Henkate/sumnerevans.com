Title: On Immigration
Date: 2017-09-07 11:01
Author: Sumner Evans
Category: Politics
Tags: Immigration
Slug: 2017-09-2017-on-immigration
Status: draft

Immigration has been on the news a lot lately. I want to dive deep and evaluate
the issue on the spiritual level, at the issue level, at the policy level, at
the political level, and at the optical level.

# Spiritual

# Issue

# Policy

# Political

# Optical
The optics of stricter immigration laws is always bad.

# Conclusion
There are very few people who care about looking past the optics of a given
issue.
