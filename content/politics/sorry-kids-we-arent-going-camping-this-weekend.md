Title: Sorry Kids, We Aren't Going Camping this Weekend
Date: 2013-10-01 05:26
Author: Sumner Evans
Category: Politics
Slug: sorry-kids-we-arent-going-camping-this-weekend
Status: published

Since the Democrats in the Senate kept on tabling the House Republicans'
reasonable requests, we are now in a shutdown. I don't think that it will be too
bad... Unless the Democrats keep their hard line on Obamacare. The Republicans
in the house have used every possible means to make sure that this shutdown
didn't happen. They withdrew their request for total defunding of Obamacare and
instead wanted only to delay it. When that was tabled by the Democrats in the
Senate, the Republicans in the House passed another bill calling for the delay
of only a few items of the Obamacare bill. Still the stubbornness of the
Democrats in the Senate and the President sent us into this shutdown.  And
unless the Democrats in the Senate and the President start to think about
working on a compromise, you won't be able to take your kids camping this
weekend.
