My Adventures in Idris
######################

:date: 2018-03-07 00:12:33
:authors: Sumner Evans
:category: Technology
:tags: Idris, Programming, Programming Languages
:slug: adventures-in-idris
:status: draft
:summary: My thoughts on the Idris programming language.

Idris is a general purpose, purely functional programming language with
dependent types.
