Title: Browser Wars: Firefox vs. Chrome
Date: 2014-02-17 11:52
Author: Sumner Evans
Category: Technology
Tags: Browser Wars, Firefox, Chrome
Slug: browser-wars-ff-vs-chrome
Status: draft

*This Browser War's format will be much the same as my previous [Browser
War](http://sumnerevans.wordpress.com/2013/03/03/browser-wars-chrome-vs-ie/)
in which I compare Chrome and IE.*

Today's comparison will probably come out a little fairer than my
previous "Browser War". Firefox and Chrome are, in my opinion, are some
of the best browsers out there. Let's get started.

<ol>
<li>
The Stats
=========

<p>
Per the statistics on [Wikipedia's
page](http://en.wikipedia.org/wiki/Usage_share_of_web_browsers#Summary_table)
Chrome has 46.60% of the market share while Firefox has only 20.37%.
Firefox is actually lower than IE (24.64%) right now. **Edge: Chrome**.

</li>
<li>
Cross-Platform Compatibility
============================
