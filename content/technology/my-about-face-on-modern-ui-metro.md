Title: My About-Face on Modern UI/Metro
Date: 2014-03-28 05:56
Author: Sumner Evans
Category: Technology
Slug: my-about-face-on-modern-ui-metro
Status: draft

A while ago I posted an article on how much I liked Windows 8 and the
Metro interface (now renamed Modern UI). That was then. This is now and
I have changed my mind. Here are my complaints about Windows 8:

1.  Apps
    ====

    It is a little sad, but it is very true: The Windows App Store just
    doesn't have nearly enough useful apps on it. It's gotten much
    better but not enough to stop my rejection of Modern UI. In a little
    experiment I performed to see how bad it actually was


