Title: At Sea (Sailing the St. Lawrence River)
Date: 2016-06-02 19:00
Author: sumner
Category: New England Trip
Tags: At Sea, Canada, St. Lawrence River, Travel
Slug: at-sea-st-lawrence-river
Status: published

Today we were at sea. I managed to stay fairly busy but it wasn't a very
exciting day. I played some chess and ping pong and went to the show, but other
than that it wasn't an interesting day. Tomorrow we are in Quebec City, Quebec,
Canada so it should be more interesting.
