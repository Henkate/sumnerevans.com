Contact
=======

You may contact me in a variety of ways. Here are my preferred methods of
communication, listed in order of how quickly you can expect to receive a
response:

* **Matrix:** |matrix|_
* **Email:** me [at] sumnerevans [dot] com
* **Social Media:** links to my social media profiles are listed below my
  profile picture.

.. |matrix| replace:: ``@sumner:sumnerevans.com``
.. _matrix: https://matrix.to/#/@sumner:sumnerevans.com

Security
--------

I prefer E2E encrypted DMs in Matrix.

I use my GPG key for signing git commits, git tags, and emails. If you have a
GPG key and would like me to encrypt my email messages to you, please let me
know.

There are a variety of ways that you can get my GPG key to verify my commits and
emails. Here are a few:

* **Keybase:** I'm jsve_ on Keybase.
* **PGP Keyservers:** You can find my `key on a keyserver <keyserver_>`_. Just
  search for my email. I recommend using http://keys.gnupg.net because the MIT
  keyserver is overloaded all the time it seems.
* **Right here:**

  My full GPG Key ID is::

      3F15 C22B FD12 5095 F9C0 7275 8904 527A B500 22FD

  You can download my public key here: |pubkey|_.

.. _jsve: https://keybase.io/jsve
.. _keyserver: http://keys.gnupg.net/pks/lookup?search=me%40sumnerevans.com&fingerprint=on&op=index
.. |pubkey| replace:: ``sumner-gpg-pubkey.asc``
.. _pubkey: /static/sumner-gpg-pubkey.asc
