About
=====

Hello, welcome to my website! I am so glad you stopped by to check it out!

Let me introduce myself. My name is Sumner, I am 22 years old and live in
Denver, Colorado. I am a Software Engineer at `The Trade Desk`_. I graduated
with my undergraduate degree in Computer Science from Colorado School of Mines
in May 2018 and then graduated with my masters degree in Computer Science from
Mines in May 2019.

Things I'm passionate about:

* Programming
* Computer Science
* Free and Open Source Software
* Self hosting and owning my data
* Teaching

I'm grateful to have a great job where I am able to exercise my love of
programming and computer science. On the side, I work on a variety of open
source projects, which you can find on my portfolio_.

I was homeschooled through high school, but I also attended Red Rocks Community
College doing a few classes a semester during high school. I also worked at
`Can/Am Technologies, Inc. <canam_>`_ as a software developer during high
school. (See my `portfolio`_ for more info).

When I'm not working, I enjoy playing soccer, volleyball, and I've been getting
into racquetball lately. During high school, I won the FRCCA State Championship
with Denver Eagles my senior year. I also used to practice TaeKwonDo regularly,
but I have not had enough time since I started college.  While I did TaeKwonDo,
I achieved my 3rd Degree Black Belt.

This site is the place to get updates on my life. I maintain a blog_ here where
I post occasionally. I post about my travels, technology, politics, etc. On my
portfolio page, I have an up-to-date overview of `my professional achievements
<portfolio_>`_. I also have an always-up-to-date link to my resume_. You can
also find links to all of my social media accounts from this site.

.. _The Trade Desk: https://www.thetradedesk.com/
.. _canam: https://canamtechnologies.com/
.. _blog: /
.. _portfolio: /pages/portfolio.html
.. _gear: /pages/gear.html
.. _resume: /static/resume.pdf

Happy reading!
