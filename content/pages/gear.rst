Gear
====

\* indicates daily driver

Computers
---------

I run Arch Linux on all of my daily drivers. I use the i3 window manager. You
can find my `full dotfiles and miscellaneous scripts on GitLab. <dotfiles_>`_

.. _dotfiles: https://gitlab.com/sumner/dotfiles.git

- **Desktop*:** A custom-built desktop. See the `part list on PCPartPicker
  <partlist_>`_. Highlights are:

  - AMD Ryzen 7 2700X
  - Nvidia RTX 2070
  - 32 GB DDR4-3200 RAM
  - 512 GB NVMe and a 1 TB SSD

- **Laptop*:** ThinkPad T580

  - Intel Core i7-8650U
  - Nvidia GeForce MX150
  - 32 GB RAM
  - 512 GB NVMe SSD

- **Laptop:** MacBook Pro: 15", Retina display, Mid 2014. I primarily use this
  as a test machine for macOS and various Linux distributions that I try.

  - 16 GB RAM
  - 512 GB SSD

Accessories
-----------

- **Monitors:** `Dell S2417DG Monitor <dells2417dg_>`_ and
  `Dell U2412M Monitor <dellu2412m_>`_

- **Keyboards:** `Pok3r 3 60% Keyboard with Cherry MX Clears <pok3r3_>`_ and
  `CODE V3 87-Key with Cherry MX Clears <code_>`_

- **Mouse:** `Logitech G300s <g300s_>`_

- **Microphone:** `TONOR Professional Studio Condenser Microphone Kit
  <tonormic_>`_

- **Laptop Dock:** `Lenovo ThinkPad USB-C UltraDock <ultradock_>`_

- **VR:** `Oculus Rift with Touch <oculus_>`_

- **Headphones:** `Audio Technica ATH-M50X <ath-m50x_>`_ and `Audio-Technica
  ATH-MSR7BK <ath-msr7bk_>`_

- **Speakers:** `Audioengine A2+ <audioengine-a2plus_>`_

- **Earbuds:** `Google Pixel Buds <pixelbuds_>`_

- **Webcam:** `Logitech HD Pro Webcam C920 <logitech_>`_

- **USB Splitter:** `UGREEN USB 3.0 Sharing Switch Selector <ugreen_>`_


Mobile Devices
--------------

- **Smartphone*:** Google Pixel 3a (64 GB, Just Black)
- **Smartphone:** Google Pixel 1 (32 GB, Quite Black)
- **Smartphone:** iPhone 5s (32 GB, Space Gray)
- **Smartwatch:** Huawei Watch Black Stainless Steel
- **Tablet:** iPad Pro 9.7 inch (128 GB)

Other Electronics
-----------------

- **Raspberry Pi 1**

.. _partlist: https://pcpartpicker.com/user/sumner/saved/#view=Lxc9Jx
.. _dells2417dg: https://www.amazon.com/dp/B01IOO4SGK
.. _dellu2412m: https://www.amazon.com/dp/B07D1JCZL2
.. _pok3r3: https://www.amazon.com/dp/B00OFM6F80
.. _code: https://www.amazon.com/dp/B07MP1PV5B
.. _g300s: https://www.amazon.com/dp/B00RH6R7C4
.. _tonormic: https://www.amazon.com/dp/B01KHMUQ2M
.. _oculus: https://www.amazon.com/dp/B073X8N1YW
.. _ultradock: https://www.amazon.com/dp/B01N9RW2A3
.. _ath-m50x: https://www.amazon.com/dp/B076BXN5MD
.. _ath-msr7bk: https://www.amazon.com/dp/B00PEU9CFA
.. _audioengine-a2plus: https://www.amazon.com/dp/B010OIVSDA
.. _pixelbuds: https://store.google.com/product/google_pixel_buds
.. _logitech: https://www.amazon.com/dp/B006JH8T3S
.. _ugreen: https://www.amazon.com/dp/B01N6GD9JO
