#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

# This file is only used if you use `make publish` or
# explicitly specify it as your config file.

import os
import sys
sys.path.append(os.curdir)
from pelicanconf import *

SITEURL = 'https://sumnerevans.com'
RELATIVE_URLS = False

FEED_ALL_ATOM = 'feeds/all.atom.xml'
CATEGORY_FEED_ATOM = 'feeds/{slug}.atom.xml'

PLUGINS += ['minify']
MINIFY = {
    'remove_comments': True,
    'reduce_boolean_attributes': True,
    'pre_tags': ['pre', 'textarea', 'code'],
}

DELETE_OUTPUT_DIRECTORY = True

# Analytics
GOOGLE_ANALYTICS = 'UA-104941015-1'

MATOMO_URL = 'matomo.sumnerevans.com'
MATOMO_SITE_ID = '1'
